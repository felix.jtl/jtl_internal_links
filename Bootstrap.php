<?php declare(strict_types=1);

namespace Plugin\jtl_internal_links;

use JTL\Catalog\Category\Kategorie;
use JTL\Catalog\Hersteller;
use JTL\Catalog\Product\Artikel;
use JTL\DB\ReturnType;
use JTL\Events\Dispatcher;
use JTL\Helpers\URL;
use JTL\Language\LanguageHelper;
use JTL\Link\Link;
use JTL\Link\LinkGroup;
use JTL\Link\LinkGroupList;
use JTL\Plugin\Bootstrapper;
use JTL\Shop;
use stdClass;

/**
 * Class Bootstrap
 * @package Plugin\jtl_internal_links
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @var int
     */
    private int $languageID = 0;

    /**
     * @var bool
     */
    private bool $isDefaultLangActive = true;

    /**
     * @var string
     */
    private string $baseURL;

    /**
     * @inheritdoc
     */
    public function boot(Dispatcher $dispatcher): void
    {
        $this->baseURL    = Shop::getURL() . '/';
        $this->languageID = Shop::getLanguageID();
        $dispatcher->listen('shop.hook.' . \HOOK_SEOCHECK_ENDE, function () {
            $this->languageID          = Shop::getLanguageID();
            $this->isDefaultLangActive = LanguageHelper::isDefaultLanguageActive();
        });
        $dispatcher->listen('shop.hook.' . \HOOK_KATEGORIE_CLASS_LOADFROMDB, function ($args) {
            /** @var Kategorie $category */
            $category = $args['oKategorie'] ?? null;
            if ($category === null || $args['cached'] === true) {
                return;
            }
            if (\method_exists($category, 'getDescription')) {
                $category->setDescription($this->parseText($category->getDescription()));
            } else {
                $category->cBeschreibung = $this->parseText($category->cBeschreibung);
            }
        });
        $dispatcher->listen('shop.hook.' . \HOOK_ARTIKEL_CLASS_FUELLEARTIKEL, function ($args) {
            /** @var Artikel $product */
            $product = $args['oArtikel'] ?? null;
            if ($product === null || $args['cached'] === true) {
                return;
            }
            $product->cBeschreibung     = $this->parseText($product->cBeschreibung);
            $product->cKurzBeschreibung = $this->parseText($product->cKurzBeschreibung);
        });
        $dispatcher->listen('shop.hook.' . \HOOK_HERSTELLER_CLASS_LOADFROMDB, function ($args) {
            /** @var Hersteller $manufacturer */
            $manufacturer = $args['oHersteller'] ?? null;
            if ($manufacturer === null || $args['cached'] === true) {
                return;
            }
            if (\method_exists($manufacturer, 'getDescription')) {
                $manufacturer->setDescription($this->parseText($manufacturer->getDescription()));
            } else {
                $manufacturer->cBeschreibung = $this->parseText($manufacturer->cBeschreibung);
            }
        });
        $dispatcher->listen('shop.hook.' . \HOOK_LINKGROUPS_LOADED_PRE_CACHE, function ($args) {
            /** @var LinkGroupList $list */
            $list       = $args['list'];
            $linkGroups = $list->getLinkGroups();
            /** @var LinkGroup $linkGroup */
            foreach ($linkGroups as $linkGroup) {
                /** @var Link $link */
                foreach ($linkGroup->getLinks() as $link) {
                    foreach ($link->getContents() as $langID => $content) {
                        $link->setContent($this->parseText($content), $langID);
                    }
                }
            }
        });
        parent::boot($dispatcher);
    }

    /**
     * Diese Funktion erhält einen Text als String und parsed ihn. Variablen die geparsed werden lauten wie folgt:
     * $#a:ID:NAME#$ => ID = kArtikel NAME => Wunschname - wird in eine URL (evt. SEO) zum Artikel umgewandelt.
     * $#k:ID:NAME#$ => ID = kKategorie NAME => Wunschname - wird in eine URL (evt. SEO) zur Kategorie umgewandelt.
     * $#h:ID:NAME#$ => ID = kHersteller NAME => Wunschname - wird in eine URL (evt. SEO) zum Hersteller umgewandelt.
     * $#m:ID:NAME#$ => ID = kMerkmalWert NAME => Wunschname - wird in eine URL (evt. SEO) zum MerkmalWert umgewandelt.
     * $#n:ID:NAME#$ => ID = kNews NAME => Wunschname - wird in eine URL (evt. SEO) zur News umgewandelt.
     * $#l:ID:NAME#$ => ID = kSuchanfrage NAME => Wunschname - wird in eine URL (evt. SEO) zur Livesuche umgewandelt.
     *
     * @param string $text
     * @return string
     */
    public function parseText($text)
    {
        if (empty($text)) {
            return $text;
        }
        \preg_match_all(
            '/\${1}\#{1}[akhmnl]{1}:[0-9]+\:{0,1}' .
            '[\w\.\,\!\"\§\$\%\&\/\(\)\=\`\´\+\~\*\'\;\-\_\?\{\}\[\]\ ]{0,}\#{1}\${1}/',
            $text,
            $hits
        );
        if (!\is_array($hits[0]) || \count($hits[0]) === 0) {
            return $text;
        }
        $params = [
            'a' => \URLART_ARTIKEL,
            'k' => \URLART_KATEGORIE,
            'h' => \URLART_HERSTELLER,
            'm' => \URLART_MERKMAL,
            'n' => \URLART_NEWS,
            'l' => \URLART_LIVESUCHE
        ];
        foreach ($hits[0] as $hit) {
            $param = \mb_substr($hit, \mb_strpos($hit, '#') + 1, 1);
            $until = \mb_strpos($hit, ':', 4);
            // Es wurde kein Name angegeben
            if ($until === false) {
                $until   = \mb_strpos($hit, ':', 3);
                $from    = \mb_strpos($hit, '#', $until);
                $keyName = \mb_substr($hit, $until + 1, ($from - 1) - $until);
                $name    = '';
            } else {
                $keyName = \mb_substr($hit, 4, $until - 4);
                $name    = \mb_substr($hit, $until + 1, \mb_strpos($hit, '#', $until) - ($until + 1));
            }

            $item   = new stdClass();
            $exists = false;
            switch ($params[$param]) {
                case \URLART_ARTIKEL:
                    $item->kArtikel = (int)$keyName;
                    $item->cKey     = 'kArtikel';
                    $table          = 'tartikel';
                    $locSQL         = '';
                    if ($this->languageID > 0 && !$this->isDefaultLangActive) {
                        $table  = 'tartikelsprache';
                        $locSQL = ' AND tartikelsprache.kSprache = ' . $this->languageID;
                    }
                    $data = $this->getDB()->query(
                        'SELECT ' . $table . '.kArtikel, ' . $table . '.cName, tseo.cSeo
                            FROM ' . $table . "
                            LEFT JOIN tseo
                                ON tseo.cKey = 'kArtikel'
                                AND tseo.kKey = " . $table . '.kArtikel
                                AND tseo.kSprache = ' . $this->languageID . '
                            WHERE ' . $table . '.kArtikel = ' . (int)$keyName . $locSQL,
                        ReturnType::SINGLE_OBJECT
                    );
                    if (isset($data->kArtikel) && $data->kArtikel > 0) {
                        $exists      = true;
                        $item->cSeo  = $data->cSeo;
                        $item->cName = !empty($data->cName) ? $data->cName : 'Link';
                    }
                    break;

                case \URLART_KATEGORIE:
                    $item->kKategorie = (int)$keyName;
                    $item->cKey       = 'kKategorie';
                    $table            = 'tkategorie';
                    $locSQL           = '';
                    if ($this->languageID > 0 && !$this->isDefaultLangActive) {
                        $table  = 'tkategoriesprache';
                        $locSQL = ' AND tkategoriesprache.kSprache = ' . $this->languageID;
                    }
                    $data = $this->getDB()->query(
                        "SELECT {$table}.kKategorie, {$table}.cName, tseo.cSeo
                            FROM {$table}
                            LEFT JOIN tseo
                                ON tseo.cKey = 'kKategorie'
                                AND tseo.kKey = {$table}.kKategorie
                                AND tseo.kSprache = {$this->languageID}
                            WHERE {$table}.kKategorie = " . (int)$keyName . $locSQL,
                        ReturnType::SINGLE_OBJECT
                    );

                    if (isset($data->kKategorie) && $data->kKategorie > 0) {
                        $exists      = true;
                        $item->cSeo  = $data->cSeo;
                        $item->cName = !empty($data->cName) ? $data->cName : 'Link';
                    }
                    break;

                case \URLART_HERSTELLER:
                    $item->kHersteller = (int)$keyName;
                    $item->cKey        = 'kHersteller';
                    $data              = $this->getDB()->getSingleObject(
                        "SELECT thersteller.kHersteller, thersteller.cName, tseo.cSeo
                            FROM thersteller
                            LEFT JOIN tseo
                                ON tseo.cKey = 'kHersteller'
                                AND tseo.kKey = thersteller.kHersteller
                                AND tseo.kSprache = :lid
                            WHERE thersteller.kHersteller = :mid",
                        ['lid' => $this->languageID, 'mid' => $item->kHersteller]
                    );

                    if (isset($data->kHersteller) && $data->kHersteller > 0) {
                        $exists      = true;
                        $item->cSeo  = $data->cSeo;
                        $item->cName = !empty($data->cName) ? $data->cName : 'Link';
                    }
                    break;

                case \URLART_MERKMAL:
                    $item->kMerkmalWert = (int)$keyName;
                    $item->cKey         = 'kMerkmalWert';
                    $data               = $this->getDB()->getSingleObject(
                        "SELECT tmerkmalwertsprache.kMerkmalWert, tmerkmalwertsprache.cWert, tseo.cSeo
                            FROM tmerkmalwertsprache
                            LEFT JOIN tseo
                                ON tseo.cKey = 'kMerkmalWert'
                                AND tseo.kKey = tmerkmalwertsprache.kMerkmalWert
                                AND tseo.kSprache = :lid
                            WHERE tmerkmalwertsprache.kMerkmalWert = :mmw
                                AND tmerkmalwertsprache.kSprache = :lid",
                        ['lid' => $this->languageID, 'mmw' => $item->kMerkmalWert]
                    );

                    if (isset($data->kMerkmalWert) && $data->kMerkmalWert > 0) {
                        $exists      = true;
                        $item->cSeo  = $data->cSeo;
                        $item->cName = !empty($data->cWert) ? $data->cWert : 'Link';
                    }
                    break;

                case \URLART_NEWS:
                    $item->kNews = (int)$keyName;
                    $item->cKey  = 'kNews';
                    $data        = $this->getDB()->getSingleObject(
                        "SELECT tnews.kNews, tseo.cSeo, tnewssprache.title
                            FROM tnews
                            JOIN tnewssprache
                                ON tnewssprache.kNews = tnews.kNews
                            LEFT JOIN tseo
                                ON tseo.cKey = 'kNews'
                                AND tseo.kKey = tnews.kNews
                                AND tseo.kSprache = :lid
                            WHERE tnews.kNews = :nid",
                        ['lid' => $this->languageID, 'nid' => $item->kNews]
                    );

                    if (isset($data->kNews) && $data->kNews > 0) {
                        $exists      = true;
                        $item->cSeo  = $data->cSeo;
                        $item->cName = empty($data->title) ? 'Link' : $data->title;
                    }
                    break;

                case \URLART_LIVESUCHE:
                    $item->kSuchanfrage = (int)$keyName;
                    $item->cKey         = 'kSuchanfrage';
                    $data               = $this->getDB()->getSingleObject(
                        "SELECT tsuchanfrage.kSuchanfrage, tsuchanfrage.cSuche, tseo.cSeo
                            FROM tsuchanfrage
                            LEFT JOIN tseo
                                ON tseo.cKey = 'kSuchanfrage'
                                AND tseo.kKey = tsuchanfrage.kSuchanfrage
                                AND tseo.kSprache = :lid
                            WHERE tsuchanfrage.kSuchanfrage = :qid",
                        ['lid' => $this->languageID, 'qid' => $item->kSuchanfrage]
                    );

                    if (isset($data->kSuchanfrage) && $data->kSuchanfrage > 0) {
                        $exists      = true;
                        $item->cSeo  = $data->cSeo;
                        $item->cName = !empty($data->cSuche) ? $data->cSuche : 'Link';
                    }
                    break;
            }
            if (\mb_strlen($name) > 0) {
                $item->cName = $name;
                $name        = ':' . $name;
            }
            if ($exists) {
                $url  = URL::buildURL($item, $params[$param], true, $this->baseURL);
                $text = \str_replace(
                    '$#' . $param . ':' . $keyName . $name . '#$',
                    '<a href="' . $url . '">' . $item->cName . '</a>',
                    $text
                );
            } else {
                $text = \str_replace(
                    '$#' . $param . ':' . $keyName . $name . '#$',
                    '<a href="' . $this->baseURL . '" >' . Shop::Lang()->get('parseTextNoLinkID') . '</a>',
                    $text
                );
            }
        }

        return $text;
    }
}
